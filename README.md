# PyPI Compare

Compare the two latest versions of a PyPI package and identify suspicious changes.

## Requirements
Python >= 3.6 

See Requirements.txt for additional Python requirements.

Windows 10 and Linux should both work.

##Current Features
* find new suspicious imports (defined by `suspicious_imports.txt`)

* compare versions and dertermine if only `setup.py` has changed

* Samples from latest run are saved in `samples.txt`

* Results are saved as a json dump and can therefore be easily used for further analysis

## Usage
```
usage: main.py [-h] [-r <filename>] [-s <int>] [-o <filename>]

Compare different PyPi-package versions and identify suspicious changes.

optional arguments:
  -h, --help            show this help message and exit
  -r <filename>, --read-samples-from-file <filename>
                        Read package list from .txt file
  -s <int>, --sample-size <int>
                        Number of randomly selected PyPi packages
  -o <filename>, --output <filename>
                        Specify file for the output of the program
```

#### Example

Run pypi_compare to analyze 10 random samples: 

`./pypi_compare -s 10`

Instruct pypi_compare to read package names from a file (text file, one line per package name):

`./pypi_compare -r file.txt`