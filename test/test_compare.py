from pathlib import Path
import pytest
import compare as cmp


def test_list_all_py_files_in_directory():
    result = cmp.list_all_py_files_in_directory(Path("dummy_package/pkg_latest"))
    expected_result = [
        Path("different.py"),
        Path("hello_world.py"),
        Path("new.py"),
        Path("setup.py"),
    ]
    assert set(result) == set(expected_result)


def test_list_all_py_files_in_directory_with_non_existing_dir():
    result = cmp.list_all_py_files_in_directory(Path("does_not_exist"))
    expected_result = []
    assert set(result) == set(expected_result)


def test_list_all_py_files_in_directory_with_non_py_file():
    result = cmp.list_all_py_files_in_directory(Path("dummy_package/pkg_previous"))
    expected_result = [
        Path("deleted.py"),
        Path("hello_world.py"),
        Path("different.py"),
        Path("setup.py"),
    ]
    assert set(result) == set(expected_result)


def test_compare():
    comparision = cmp.PyPiComparison(
        Path("dummy_package/pkg_latest"), Path("dummy_package/pkg_previous"), "test"
    )
    comparision.compare()
    assert set(comparision.new_files) == {Path("new.py")}
    assert set(comparision.deleted_files) == {Path("deleted.py")}
    assert set(comparision.common_files) == {
        Path("hello_world.py"),
        Path("different.py"),
        Path("setup.py"),
    }
    assert set(comparision.modified_files) == {Path("different.py")}


def test__find_setup_py():
    directory = Path("dummy_package/pkg_latest")
    expected_result = Path("dummy_package/pkg_latest/setup.py")
    assert cmp._find_setup_py(directory) == expected_result


@pytest.mark.parametrize("directory", [Path("dummy_package/"), Path("nonexisting/path")])
def test__find_setup_py_exception(directory):
    with pytest.raises(FileNotFoundError):
        cmp._find_setup_py(directory)


def test_is_only_setup_changed_yes():
    comparision = cmp.PyPiComparison(
        Path("dummy_package_setup_change/latest"),
        Path("dummy_package_setup_change/previous"),
        "test",
    )
    comparision.compare()
    assert comparision.is_only_setup_py_changed()


def test_is_only_setup_changed_no():
    comparision = cmp.PyPiComparison(
        Path("dummy_package/pkg_latest"), Path("dummy_package/pkg_previous"), "test"
    )
    comparision.compare()
    assert not comparision.is_only_setup_py_changed()


def test_compare_setup_py():
    comparision = cmp.PyPiComparison(
        Path("dummy_package/pkg_latest"), Path("dummy_package/pkg_previous"), "test"
    )
    assert comparision.compare_setup_py() == []


@pytest.mark.parametrize(
    "directory, file, lines",
    [
        (
            Path("dummy_package/pkg_latest/"),
            Path("dummy_package/pkg_latest/setup.py"),
            ['"fake_setup"', ""],
        ),
        (
            Path("dummy_package/pkg_previous/"),
            Path("dummy_package/pkg_previous/setup.py"),
            ['"fake_setup"', ""],
        ),
        (Path("non/existing"), Path(""), []),
    ],
)
def test__find_and_read_setup_py(directory, file, lines):
    assert cmp._find_and_read_setup_py(directory) == (file, lines)


def test_read_file_should_work():
    assert cmp.read_file(Path("dummy_package/pkg_latest/new.py")) == 'print("i am new.")'


def test__find_lines_containing_import():
    lines = [
        "import os\n",
        "import project.cool\n",
        "from pathlib import Path\n",
        "print('hi world')\n",
    ]
    expected_result = ["import os\n", "import project.cool\n", "from pathlib import Path\n"]
    assert cmp._find_lines_containing_import(lines) == expected_result


@pytest.mark.parametrize(
    "line, expected_module",
    [
        ("import os\n", "os"),
        ("import project.cool\n", "project"),
        ("from pathlib import Path", "pathlib"),
        ("print('hi world')", None),
        ("import os, socket", ["os", "socket"])
    ],
)
def test__find_import(line, expected_module):
    assert cmp._find_import(line) == expected_module


@pytest.mark.parametrize(
    "file, expected_output",
    [
        (Path("dummy_package/pkg_latest/different.py"), ["os", "project", "pathlib"]),
        (Path("dummy_package/pkg_latest/new.py"), []),
        (Path("dummy_single_files/import_test.py"), ["os", "pathlib", "socket", "subprocess", "pytest", "compare"])
    ],
)
def test_find_all_imports(file, expected_output):
    assert cmp.find_all_imports(file) == expected_output


@pytest.mark.parametrize(
    "line, expected_result",
    [
        ("import os # comment", False),
        ("from sys import exit", False),
        ("# import path", True),
        ("'''from os import system'''", True),
        ('"""import pytest"""', True),
    ],
)
def test__is_import_commented_out(line, expected_result):
    assert cmp._is_import_commented_out(line) == expected_result


@pytest.mark.parametrize(
    "diff",
    [
        [
            "--- C:\\Users\\user\\AppData\\Local\\Temp\\tmpo63rtp0u\\previous\\miniman-0.0.5\\setup.py\n",
            "\n",
            "+++ C:\\Users\\user\\AppData\\Local\\Temp\\tmpo63rtp0u\\latest\\miniman-0.0.8\\setup.py\n",
            "\n",
            "@@ -5,7 +5,7 @@\n",
            "\n",
            "\n",
            " setup(\n",
            '     name="miniman",\n',
            '-    version="0.0.5",\n',
            '+    version="0.0.8",\n',
            '     description="minimal manpages.",\n',
            "     long_description=long_description,\n",
            '     long_description_content_type="text/markdown",\n',
        ],
        [
            "--- C:\\Users\\user\\AppData\\Local\\Temp\\tmpo63rtp0u\\previous\\miniman-0.0.5\\setup.py\n",
            "\n",
            "+++ C:\\Users\\user\\AppData\\Local\\Temp\\tmpo63rtp0u\\latest\\miniman-0.0.8\\setup.py\n",
            "\n",
            "@@ -5,7 +5,7 @@\n",
            "\n",
            "\n",
            " setup(\n",
            '-     name="miniman",\n',
            '+     name="miniman2",\n',
            '     version="0.0.5",\n',
            '+    description="minimal manpages.",\n',
            "     long_description=long_description,\n",
            '     long_description_content_type="text/markdown",\n',
        ],
    ],
)
def test_benign_setup_changes_should_be_detected_as_benign(diff):
    assert cmp.are_setup_changes_benign(diff)


@pytest.mark.parametrize(
    "diff",
    [
        [
            "--- C:\\Users\\Paul\\AppData\\Local\\Temp\\tmptq6o0i_b\\previous\\Products.PloneTemplates-1.0.2\\setup.py\n",
            "\n",
            "@@ -1,39 +1,28 @@\n",
            "\n",
            "-import os\n",
            "-from setuptools import setup\n",
            "+from setuptools import setup, find_packages\n",
            "\n",
            "-\n",
            "-CLASSIFIERS = [\n",
            "-    'Programming Language :: Python',\n",
            "-    'Framework :: Zope2',\n",
            "-    'Framework :: Plone',\n",
            "-]\n",
            "-\n",
            "-version = '1.0.2'\n",
            "-\n",
            "-readme_file= os.path.join('Products', 'PloneTemplates', 'readme.txt')\n",
            "-desc = open(readme_file).read().strip()\n",
            "-changes_file = os.path.join('Products', 'PloneTemplates', 'releasenotes.txt')\n",
            "-changes = open(changes_file).read().strip()\n",
            "-\n",
            "+version = '1.0.3'\n",
            "+      description='Content templates for Plone'\n",
            "+      long_description=open('README.txt').read() + '\\n' +\n",
            "+                       open('CHANGES.txt').read(),\n",
            "+      classifiers=[\n",
            "+          'Programming Language :: Python',\n",
            "+          'Framework :: Zope2',\n",
            "+      ],\n",
            "+      packages=find_packages(exclude=['ez_setup']),\n",
            "+      install_requires=[\n",
            "+        'setuptools',\n",
            "+      ],\n",
            "       )\n",
        ]
    ],
)
def test_non_benign_setup_changes_should_not_be_detected_as_benign(diff):
    assert not cmp.are_setup_changes_benign(diff)
