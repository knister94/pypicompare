import json
from pathlib import Path
import socket
import pytest
import pypi_compare


def is_pypi_reachable():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.connect(("pypi.org", 443))
            s.close()
        except (TimeoutError, socket.gaierror):
            return False
        return True


@pytest.mark.parametrize(
    "urls, is_sane",
    [
        (["www.some-server.com"], False),
        (["https://some-server.com/file.zip", "https://some-server.com/file2.zip"], True),
        (["http://google.com", "http://bing.com", "http://duckduckgo.com"], False),
        (["http://server.com/archive.tar.gz", "http://server.com/archive.whl"], False),
        (["https://server.com/hotwheel.whl", "http://server.com/new-hotwheel.whl"], True),
    ],
)
def test_are_urls_sane(urls, is_sane):
    assert pypi_compare.are_urls_sane(urls) == is_sane


def test_is_zip_file_zip():
    assert pypi_compare.is_zip_file("hello.zip")


def test_is_zip_file_whl():
    assert pypi_compare.is_zip_file("hello.whl")


def test_is_zip_file_egg():
    assert pypi_compare.is_zip_file("dir/hello.egg")


def test_is_zip_file_invalid():
    assert not pypi_compare.is_zip_file("archive.tar.gz")


@pytest.mark.parametrize(
    "filename, out",
    [
        ("hello.zip", "hello"),
        ("archives/somefile.tar.gz", "archives/somefile"),
        ("file.txt", "file.txt"),
        ("", ""),
        ("a/b.whl", "a/b"),
    ],
)
def test_remove_archive_extension(filename, out):
    assert pypi_compare.remove_archive_extension(filename) == out


@pytest.mark.skipif(not is_pypi_reachable(), reason="PyPI not reachable. Skipping...")
def test_get_all_pypi_names():
    names = pypi_compare.get_all_pypi_names()
    assert len(names) > 0


def test_select_random_samples():
    samples = pypi_compare.select_random_samples(10)
    assert len(samples) == 10


def test__check_enough_packages_left():
    with pytest.raises(ValueError):
        pypi_compare._check_enough_packages_left(1, 2)


@pytest.mark.skipif(not is_pypi_reachable(), reason="PyPI not reachable. Skipping...")
def test_get_latest_versions_urls():
    urls = pypi_compare.get_latest_versions_urls("urllib3")
    assert len(urls) == 2
    assert "http" in urls[0] and "http" in urls[1]


def test_extract_tar_gz_archive():
    pypi_compare.extract_tar_gz_archive("archives/test.tar.gz")
    assert Path("test/test.txt").is_file()
    Path("test/test.txt").unlink()
    Path("test/").rmdir()


@pytest.mark.parametrize(
    "filename", ["archives/test.egg", "archives/test.whl", "archives/test.zip"]
)
def test_extract_zip_file(filename):
    pypi_compare.extract_zip_archive(filename)
    assert Path("test/test.txt").is_file()
    Path("test/test.txt").unlink()
    Path("test/").rmdir()


@pytest.mark.skipif(not is_pypi_reachable(), reason="PyPI not reachable. Skipping...")
def test_read_samples_from_file_count():
    samples = pypi_compare.read_samples_from_file("test_samples.txt")
    assert len(samples) == 3


@pytest.mark.skipif(not is_pypi_reachable(), reason="PyPI not reachable. Skipping...")
def test_read_samples_from_file_names():
    samples = pypi_compare.read_samples_from_file("test_samples.txt")
    assert samples[0].name == "flask"
    assert samples[1].name == "urllib3"
    assert samples[2].name == "numpy"


@pytest.mark.skipif(not is_pypi_reachable(), reason="PyPI not reachable. Skipping...")
def test_read_samples_from_file_urls():
    samples = pypi_compare.read_samples_from_file("test_samples.txt")
    assert samples[0].latest_version_url is not None
    assert samples[1].prev_version_url is not None
    assert samples[2].latest_version_url is not None
    assert "https" in samples[0].latest_version_url
    assert "https" in samples[2].latest_version_url


@pytest.fixture(scope="function")
def results():
    results = [
        dict(name="test", value=12),
        dict(name="test2", value="abc"),
        dict(name="test3", value=False),
    ]
    yield results
    Path("result.log").unlink()


def test_log_result_file_exists(results):
    pypi_compare.log_result(results, "result.log")
    assert Path("result.log").is_file()


def test_log_result_file_content(results):
    pypi_compare.log_result(results, "result.log")
    assert json.loads(Path("result.log").read_text()) == results


@pytest.fixture(scope="function")
def samples():
    samples = [
        pypi_compare.PyPiSample(
            "test1", "https://someurl.com/latest.zip", "https://someurl.com/previous.zip"
        ),
        pypi_compare.PyPiSample(
            "test2", "https://someurl.com/dir/latest.tar.gz", "https://someurl.com/previous.tar.gz"
        ),
        pypi_compare.PyPiSample(
            "test3",
            "https://someurl.com/latest.egg",
            "https://someurl.com/subdir/dir/previous.whl",
        ),
    ]
    yield samples
    Path("samples.txt").unlink()


def test_log_samples_file_exists(samples):
    pypi_compare.log_samples(samples)
    assert Path("samples.txt").is_file()


def test_log_samples_file_content(samples):
    pypi_compare.log_samples(samples)
    assert Path("samples.txt").read_text() == "test1\ntest2\ntest3\n"
