#!/usr/bin/env python3

import argparse
import json
from pathlib import Path
import random
import sys
import tarfile
from tempfile import TemporaryDirectory
from time import sleep
import zipfile
import requests


from tqdm import tqdm
import compare






class PyPiSample:
    def __init__(self, name, latest_version_url, prev_version_url):
        self.name = name
        self.latest_version_url = latest_version_url
        self.prev_version_url = prev_version_url
        self.latest_dir = None
        self.previous_dir = None
        self.tempdir = None
        self.previous_archive = None
        self.latest_archive = None

    def __str__(self):
        string = f"<{self.name}>"
        return string

    def __repr__(self):
        return self.__str__()

    def download_and_extract(self):
        self.tempdir = TemporaryDirectory()
        self.latest_archive = download_file(self.latest_version_url, Path(self.tempdir.name))
        self.previous_archive = download_file(self.prev_version_url, Path(self.tempdir.name))
        self.latest_dir = extract_archive(self.latest_archive, Path(self.tempdir.name) / "latest")
        self.previous_dir = extract_archive(
            self.previous_archive, Path(self.tempdir.name) / "previous"
        )

    def cleanup(self):
        if self.tempdir:
            try:
                self.tempdir.cleanup()
            except PermissionError:
                # Attempted fix for weird permission error, randomly happening on win 10
                print(f"{self.tempdir.name} could not be deleted. Trying again...")
                sleep(1)
                try:
                    self.tempdir.cleanup()
                except PermissionError:
                    print(f"{self.tempdir.name} could not be deleted due to permission problem.")


def get_all_pypi_names():
    response = str(requests.get("https://pypi.org/simple/").content).split("\\n")
    names = []
    for line in response:
        if "</a>" in line:
            index1 = line.find(">") + 1
            index2 = line.find("</a>")
            names.append(line[index1:index2])
    return names


def get_latest_versions_urls(package_name):
    """See https://warehouse.readthedocs.io/api-reference/json/ for details on json api"""

    releases = _get_releases_from_json(package_name)
    if releases is None:
        return None
    release_versions = list(releases.keys())
    release_versions.sort()
    latest = releases[release_versions[-1]]
    previous = releases[release_versions[-2]]
    try:
        return [latest[-1]["url"], previous[-1]["url"]]
    except IndexError:
        return None


def _get_releases_from_json(package_name):
    """See https://warehouse.readthedocs.io/api-reference/json/ for details on json api"""
    try:
        response = json.loads(requests.get(f"https://pypi.org/pypi/{package_name}/json").content)
    except json.decoder.JSONDecodeError:
        return None
    releases = response["releases"]
    if len(releases) <= 1:
        return None
    else:
        return releases


def select_random_samples(sample_size):
    remaining_pypi_packages = get_all_pypi_names()
    useless_samples = []
    samples = []

    with tqdm(total=sample_size, unit=" samples", desc="Selecting packages") as progress_bar:
        while len(samples) < sample_size:

            _check_enough_packages_left(len(remaining_pypi_packages), sample_size - len(samples))

            remaining_pypi_packages = list(set(remaining_pypi_packages) - set(useless_samples))
            random_sample_names = random.choices(
                remaining_pypi_packages, k=sample_size - len(samples)
            )

            for package_name in random_sample_names:
                urls = get_latest_versions_urls(package_name)
                if are_urls_sane(urls):
                    samples.append(PyPiSample(package_name, urls[0], urls[1]))
                    progress_bar.update(1)
                else:
                    useless_samples.append(package_name)

    return samples


def _check_enough_packages_left(number_potential_samples, number_samples_to_select):
    if number_potential_samples - number_samples_to_select < 0:
        raise ValueError("Samplesize is bigger than number of available PYPI Packets!")


def are_urls_sane(urls):
    if urls is None or len(urls) != 2:
        return False
    if "http" not in urls[0] or "http" not in urls[1]:
        return False
    file_types = [urls[0].split(".")[-1], urls[1].split(".")[-1]]

    if file_types[0] != file_types[1]:  # don't compare .whl with tar.gz or .egg
        return False
    return True


def download_file(url, directory=""):
    filename = Path(f"{directory}/{url.split('/')[-1]}")
    response = requests.get(url).content
    filename.write_bytes(response)
    return str(filename)


def extract_tar_gz_archive(filename, directory=""):
    with tarfile.open(filename, "r:gz") as f:
        f.extractall(directory)
    return Path(directory)


def extract_zip_archive(filename, directory=""):
    with zipfile.ZipFile(filename, "r") as f:
        f.extractall(directory)
    return Path(directory)


def is_zip_file(filename):
    return filename.endswith(".zip") or filename.endswith(".whl") or filename.endswith(".egg")


def extract_archive(filename, directory=""):
    if is_zip_file(filename):
        return extract_zip_archive(filename, directory)
    if filename.endswith(".tar.gz"):
        return extract_tar_gz_archive(filename, directory)
    else:
        raise NotImplementedError(f"{filename} could not be extracted!")


def remove_archive_extension(filename):
    file_extensions = [".tar.gz", ".zip", ".whl", ".egg"]
    for extension in file_extensions:
        filename = filename.replace(extension, "")
    return filename


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Compare different PyPi-package versions and identify suspicious changes."
    )
    parser.add_argument(
        "-r",
        "--read-samples-from-file",
        metavar="<filename>",
        help="Read package list from .txt file",
    )
    parser.add_argument(
        "-s",
        "--sample-size",
        metavar="<int>",
        type=int,
        help="Number of randomly selected PyPi packages",
    )
    parser.add_argument(
        "-o",
        "--output",
        metavar="<filename>",
        default="result.log",
        help="Specify file for the output of the program",
    )
    args = parser.parse_args()
    if not args.sample_size and not args.read_samples_from_file:
        parser.print_help()
        sys.exit()
    return args


def read_samples_from_file(filename):
    package_names = compare.read_file(filename).split("\n")

    if not package_names[-1]:
        package_names = package_names[:-1]

    samples = []
    for name in tqdm(package_names, unit=" samples", desc="Gathering urls"):
        urls = get_latest_versions_urls(name)
        if are_urls_sane(urls):
            samples.append(PyPiSample(name, urls[0], urls[1]))
        else:
            print(f"{name} dropped from analysis, could not find useable download urls.")
    return samples


def analyze_samples(samples):
    results = []
    for sample in tqdm(samples, desc="Analyzing samples", unit=" samples"):
        result = dict(
            name=sample.name,
            url_latest=sample.latest_version_url,
            url_previous=sample.prev_version_url,
            error=None,
        )
        try:
            sample.download_and_extract()
        except (NotImplementedError, OSError, FileNotFoundError) as e:
            result.update({"error": str(type(e))})
            results.append(result)
            sample.cleanup()
            continue
        comp = compare.PyPiComparison(sample.latest_dir, sample.previous_dir, sample.name)
        result.update(comp.make_report_as_dict())
        results.append(result)
        sample.cleanup()

    return results


def print_result(results):
    errors = []
    total_setup = 0
    total_setup_benign = 0
    total_suspicious_imports = 0
    print(f"Total samples: {len(results)}\n")
    print("Suspicious samples:")

    for result in results:
        if result["error"]:
            errors.append(result)
            continue

        if result["setup_changed_only"]:
            print(
                f"{result['name']}: has only changes in setup.py."
                f" Changes are benign: {result['setup_changes_benign']}"
            )
            total_setup += 1
            if result["setup_changes_benign"]:
                total_setup_benign += 1

        if len(result["suspicious_new_imports"]) > 0:
            print(f"{result['name']}: new suspicious imports {result['suspicious_new_imports']}")
            total_suspicious_imports += 1

    _print_totals(len(results), total_setup, total_suspicious_imports, total_setup_benign)


def _print_totals(total_samples, total_setup, total_import, total_setup_benign):
    print("\nRESULTS:\n__________________________")
    print(f"Total samples analyzed: {total_samples}")
    if total_samples > 0:
        print(
            f"{total_setup} ({round(total_setup/total_samples*100, 2)}%) packages had only changes to setup.py"
        )
    if total_setup > 0:
        print(
            f"{total_setup_benign} out of {total_setup} ({round(total_setup_benign/total_setup*100, 2)}%)"
            f" only had benign changes to setup.py."
        )
    if total_samples > 0:
        print(
            f"{total_import} ({round(total_import/total_samples*100, 2)}%) had suspicious new imports"
        )


def log_result(results, filename):
    with open(filename, "w") as f:
        json.dump(results, f, indent=4)


def log_samples(samples):
    with open("samples.txt", "w") as f:
        for sample in samples:
            f.write(f"{sample.name}\n")


def main():
    args = parse_arguments()

    if args.read_samples_from_file is not None:
        samples = read_samples_from_file(args.read_samples_from_file)
    else:
        samples = select_random_samples(args.sample_size)

    results = analyze_samples(samples)
    log_result(results, args.output)
    log_samples(samples)
    print_result(results)


if __name__ == "__main__":
    main()
