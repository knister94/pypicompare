import filecmp
from pathlib import Path
from difflib import unified_diff
import chardet



class PyPiComparison:
    def __init__(self, latest_dir, previous_dir, name):
        try:
            self.latest_dir = Path(_find_setup_py(latest_dir)).parent
        except FileNotFoundError:
            self.latest_dir = latest_dir
        try:
            self.previous_dir = Path(_find_setup_py(previous_dir)).parent
        except FileNotFoundError:
            self.previous_dir = previous_dir
        self.package_name = name
        self.latest_files = []
        self.previous_files = []
        self.new_files = []
        self.deleted_files = []
        self.common_files = []
        self.modified_files = []

    def compare(self):
        self.latest_files = set(list_all_py_files_in_directory(self.latest_dir))
        self.previous_files = set(list_all_py_files_in_directory(self.previous_dir))
        self.new_files = list(self.latest_files - self.previous_files)
        self.deleted_files = list(self.previous_files - self.latest_files)
        self.common_files = list(self.latest_files & self.previous_files)
        self._find_modified_files()

    def _find_modified_files(self):
        for file in self.common_files:
            if not filecmp.cmp(self.latest_dir / file, self.previous_dir / file, shallow=False):
                self.modified_files.append(file)

    def make_report_as_dict(self):
        report = dict(name=self.package_name)
        if len(self.latest_files) == 0:
            self.compare()
        report.update({"setup_changed_only": self.is_only_setup_py_changed()})
        report.update({"new_imports": self.find_new_imports()})
        report.update({"suspicious_new_imports": self.find_suspicious_new_imports()})
        if self.is_only_setup_py_changed():
            diff = self.compare_setup_py()
            report.update({"setup_diff": "\n".join(diff)})
            report.update({"setup_changes_benign": are_setup_changes_benign(diff)})
        return report

    def is_only_setup_py_changed(self):
        differences = self.deleted_files + self.new_files + self.modified_files
        return len(differences) == 1 and differences[0].name == "setup.py"

    def compare_setup_py(self):
        setup_latest, setup_latest_lines = _find_and_read_setup_py(self.latest_dir)
        setup_previous, setup_previous_lines = _find_and_read_setup_py(self.previous_dir)
        diff = unified_diff(
            setup_previous_lines,
            setup_latest_lines,
            fromfile=str(setup_previous),
            tofile=str(setup_latest),
        )
        differences = []
        for d in diff:
            differences.append(d)
        return differences

    def find_new_imports(self):
        imports_previous = []
        imports_latest = []
        for file in self.modified_files:
            imports_previous.extend(find_all_imports(self.previous_dir / file))
            imports_latest.extend(find_all_imports(self.latest_dir / file))

        for file in list(set(self.common_files) - set(self.modified_files)):
            imports_latest.extend(find_all_imports(self.latest_dir / file))
            imports_previous.extend(find_all_imports(self.previous_dir / file))

        for file in self.new_files:
            imports_latest.extend(find_all_imports(self.latest_dir / file))

        for file in self.deleted_files:
            imports_previous.extend(find_all_imports(self.previous_dir / file))

        return list(set(imports_latest) - set(imports_previous))

    def find_suspicious_new_imports(self):
        suspicious_imports = read_file("suspicious_imports.txt").split("\n")
        new_imports = self.find_new_imports()
        found_imports = []
        for module in suspicious_imports:
            if module in new_imports:
                found_imports.append(module)
        return found_imports


def _find_setup_py(directory):
    setup = list(directory.rglob("setup.py"))
    if len(setup) == 1:
        return setup[0]
    else:
        raise FileNotFoundError(
            f"Could not find setup.py in {directory} or found multiple setup.py"
        )


def are_setup_changes_benign(diff):
    for line in diff:
        if not _is_change_addition(line):
            continue
        line = line[1:].lstrip().rstrip()
        if not _is_change_benign(line):
            return False
    return True


def _is_change_addition(line):
    if not line.startswith("+++"):
        return line.startswith("+")
    return False


def _is_change_benign(line):
    allowed_field_names = [
        "description",
        "version",
        "long_description",
        "name",
        "author",
        "author_email",
        "license",
    ]
    if len(line) == 0:
        return True
    if _is_line_only_non_alpha_numeric(line):
        return True
    line = line.split("=")
    if len(line) == 2:
        if line[0].rstrip() in allowed_field_names:
            return True
    return False


def _is_line_only_non_alpha_numeric(line):
    non_alpha_num = ["(", ")", "[", "]", ",", "{", "}"]
    count_non_alpha_num = 0
    for character in line:
        if character in non_alpha_num:
            count_non_alpha_num += 1
    if count_non_alpha_num == len(line):
        return True
    return False


def list_all_py_files_in_directory(directory):
    all_files = list(directory.rglob("*.py"))
    result = []
    for path in all_files:
        result.append(_remove_base_path_from_path(path, directory))
    return result


def _remove_base_path_from_path(path, basepath):
    path_parts = path.parts
    return Path("").joinpath(*path_parts[len(basepath.parts) :])


def read_file(filename):
    try:
        return Path(filename).read_text(encoding="utf-8")
    except UnicodeDecodeError:
        content = Path(filename).read_bytes()
        result = chardet.detect(content)
        try:
            return Path(filename).read_text(encoding=result["encoding"])
        except Exception as e:
            print(e)
            print(f"Could not decode {filename}")
            print(f"guessed encoding: {result}")
        return ""


def find_all_imports(filename):
    file = read_file(filename).split("\n")
    import_lines = _find_lines_containing_import(file)
    imports = []
    for import_line in import_lines:
        dependency = _find_import(import_line)
        if isinstance(dependency, list):
            imports.extend(dependency)
        else:
            imports.append(dependency)
    return imports


def _find_lines_containing_import(lines):
    import_lines = []
    for line in lines:
        if "import" in line:
            import_lines.append(line)
    return import_lines


def _find_import(line):
    if "import " not in line:
        return None
    if _is_import_commented_out(line):
        return None
    if "from " in line:
        words = line.split()
        try:
            package = words[words.index("from") + 1]
        except ValueError:
            return None
    else:
        line = _strip_possible_partial_comment(line)
        words = line.split()
        if len(words) > 2:
            return [word.replace(",", "") for word in words[1:]]
        if len(words) < 2:
            return None
        package = words[1]
    return package.split(".")[0]  # get root package name


def _is_import_commented_out(line):
    indicators = ["#", "'", '"']
    for symbol in indicators:
        if symbol in line:
            if line.index(symbol) < line.index("import"):
                return True
    return False


def _strip_possible_partial_comment(line):
    try:
        line = line[:line.index("#")]
    except ValueError:
        pass
    try:
        line = line[:line.index("'''")]
    except ValueError:
        pass
    try:
        line = line[:line.index('"""')]
    except ValueError:
        pass
    return line


def _find_and_read_setup_py(directory):
    try:
        setup_file = _find_setup_py(directory)
        setup_lines = read_file(setup_file).split("\n")
    except FileNotFoundError:
        setup_file = Path("")
        setup_lines = []

    return setup_file, setup_lines
